const mongoClient = require('mongodb').MongoClient;
const url = 'mongodb://localhost:27017';

const DBName               = "Hafifa";
const bookCollectionName   = "Books";
const authorCollectionName = "Authors";

const addBook = (dbObject, name, description, publishDate, author, numOfPages) => {
    const newBook = {
        name: name,
        description: description,
        publishDate: publishDate,
        author: author,
        numOfPages: numOfPages
    };

    dbObject.collection(bookCollectionName).insertOne(newBook, (err, res) => {
        if (err) { throw err };
        console.log(`The book [${newBook.name}] was inserted into the [${bookCollectionName}] collection.`);
    })
}

const addAuthor = (dbObject, firstName, lastName, birthYear) => {
    const newAuthor = {
        firstName: firstName,
        lastName: lastName,
        birthYear: birthYear
    };

    dbObject.collection(authorCollectionName).insertOne(newAuthor, (err, res) => {
        if (err) { throw err; }
        console.log(`The author [${newAuthor.firstName} ${newAuthor.lastName}] 
                    was inserted into the [${authorCollectionName}] collection.`);
    })
}

const getBooksByAuthor = (dbObject, author, callback) => {
    const regex = new RegExp(author);
    const query = {author: regex };
    dbObject.collection(bookCollectionName).find(query).toArray((err, result) => {
        if (err) { throw err; }
        callback(result);
    })
}

const getBookByName = (dbObject, name, callback) => {
    const query = { name: new RegExp(name, 'i') };

    dbObject.collection(bookCollectionName).findOne(query, (err, result) => {
        if (err) { throw err; }
        callback(result);
    });
}

const getBookByDescription = (dbObject, description, callback) => {
    const query = { description: new RegExp(description, 'i') };

    dbObject.collection(bookCollectionName).findOne(query, (err, result) => {
        if (err) { throw err; }
        callback(result);
    })
}

const getBooksByPages = (dbObject, minPageNum, callback) => {
    const query = { numOfPages: { $gte: minPageNum } };
    const sortQuery = { numOfPages: 1 };
    
    dbObject.collection(bookCollectionName).find(query).sort(sortQuery).toArray((err, res) => {
        if (err) throw err;
        callback(res);
    });
}

const getBooksSpecific = (dbObject, callback) => {
    dbObject.collection(bookCollectionName).aggregate([
        { $match: {
            name        : /^p/i,
            numOfPages  : { $gt: 200 },
            publishDate : { $gte: new Date(2015, 1, 1),
                           $lte: new Date(2020, 1, 1)}
        }},
        { $project: {
            name    : 1,
            author  : 1
        }}
    ]).toArray((err, res) => {
        callback(res);
    });
}

mongoClient.connect(url, (err, client) => {
    if (err) throw err;
    const dbObject = client.db(DBName);
    
    getBooksByAuthor(dbObject, "Herman Melville", (result) => {
        console.log(`results: ${JSON.stringify(result)}`);
    })

    getBookByName(dbObject, 'Mob', (result) => {
        console.log(`book: ${JSON.stringify(result)}`);
    })

    getBookByDescription(dbObject, 'piz', (result) => {
        console.log(`book: ${JSON.stringify(result)}`);
    })

    getBooksByPages(dbObject, 250, (result) => {
        console.log(`books: ${JSON.stringify(result)}`);
    })

    getBooksSpecific(dbObject, (result) => {
        console.log(`specific: ${JSON.stringify(result)}`);
    })

    client.close();
})